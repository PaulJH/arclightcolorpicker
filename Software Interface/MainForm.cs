﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Management;

namespace Software_Interface {
	public partial class MainForm : Form {
		private SerialPort serialPort1;
		private Dictionary<string, int[]> currSettings;
		private Dictionary<string, int[]> savedSettings;
		private string currFileSave;
		private bool failed;

		public MainForm() {
			InitializeComponent();
			string portNumb = AutodetectArduinoPort();
			if (portNumb != null)
				serialPort1 = new SerialPort(portNumb, 115200); // TODO fill this with proper port and stuff
			else
			{
				serialPort1 = new SerialPort("COM30", 115200);
				failed = true;
			}
//			serialPort1.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
			currSettings = new Dictionary<string, int[]>();
			currSettings.Add(ArcConstants.RgbSlider, null);
			currSettings.Add(ArcConstants.WhiteTemp, null);
			currSettings.Add(ArcConstants.WhiteTempValue, null);
			currSettings.Add(ArcConstants.ColorPicker, null);
			currSettings.Add(ArcConstants.ColorPickCustoms, null);
			currSettings.Add(ArcConstants.ModWheel, null);
			currSettings.Add(ArcConstants.ModWheelAmb, null);
			currSettings.Add(ArcConstants.AmbWidth, null);
			currSettings.Add(ArcConstants.ModWheelValues,null);
			currSettings.Add(ArcConstants.ModWheelAmbValues,null);
			this.Load += MainForm_Load;
		}

		private void MainForm_Load(object sender, EventArgs e) {
			/*var c = GetAll(this);
			foreach (var control in c)
			{
				if (control is NumericUpDown)
				{
					var nc = control as NumericUpDown;
					nc.ValueChanged += ValueChanged;
				}
				/*else if (control is TrackBar)
				{
					var nc = control as TrackBar;
					nc.ValueChanged += ValueChanged;
				}#1#
				else if (control is TabControl)
				{
					var nc = control as TabControl;
					nc.SelectedIndexChanged += ValueChanged;
				}
				
			}*/
			if (failed)
			{
				WriteToDebugWindow("Failed to Find arduino picked COM30 as default");
			}
			else
			{
				WriteToDebugWindow("Arduino on: " + serialPort1.PortName);
			}
			UpdateSettings();
		}
		public IEnumerable<Control> GetAll(Control control) {
			var controls = control.Controls.Cast<Control>();

			return controls.SelectMany(ctrl => GetAll(ctrl))
									  .Concat(controls);
		}
		// updates a numericUpDown with its trackBar partner
		public void UpdatePair(bool numChanged, NumericUpDown num, TrackBar track) {
			int newVal = numChanged ? (int) num.Value : track.Value;
			if (numChanged)
				track.Value = newVal;
			else
				num.Value = newVal;
		}

		//updates the color on the RGBA slider page
		public void UpdateRGBColor() {
			int[] rgba = GetColorRGBSlider();
			Color c = Color.FromArgb(rgba[3], rgba[0], rgba[1], rgba[2]);
			picRGBDisplayColor.BackColor = c;

		}

		//updates the color on the White temp page
		public void UpdateWhiteColor() {
			int[] rgba = GetColorWhiteTemp();
			Color c = Color.FromArgb(rgba[3], rgba[0], rgba[1], rgba[2]);
			picWhiteDisplayColor.BackColor = c;
		}

		//updates the color on the colorpicker page
		public void UpdateColorPickerColor() {
			int[] rgba = GetColorPicker();
			Color c = Color.FromArgb(rgba[3], rgba[0], rgba[1], rgba[2]);
			picColorPickerDisplayColor.BackColor = c;
		}

		// returns a RGBA array of ints based on the current selected page
		public int[] GetColor() {
			TabControl tc = tabControl1;
			switch (tc.SelectedIndex)
			{
				case 0:
					return GetColorRGBSlider();
				case 1:
					return GetColorWhiteTemp();
				case 2:
					return GetColorPicker();
				case 3:
					return GetColorModWheel();
				default:
					return null;
			}
		}

		// gets a RGBA from the sliders on the RGBA slider page
		public int[] GetColorRGBSlider() {
			int red = trackRed.Value;
			int green = trackGreen.Value;
			int blue = trackBlue.Value;
			int bright = trackBright.Value;
			return new[] {red, green, blue, bright};
		}

		// get a RGBA from the white temp page
		// algorithm from http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/
		public int[] GetColorWhiteTemp() {
			int temp = trackWhite.Value / 100;
			double red, green, blue;
			// calc red
			if(temp <= 66) {
				red = 255;
			}
			else {
				red = temp - 60;
				red = 329.698727446 * Math.Pow(red, -0.1332047592);
				if(red < 0)
					red = 0;
				if(red > 255)
					red = 255;
			}

			//calc green
			if(temp <= 66) {
				green = temp;
				green = 99.4708025861 * Math.Log(green) - 161.1195681661;
				if(green < 0)
					green = 0;
				if(green > 255)
					green = 255;
			}
			else {
				green = temp - 60;
				green = 288.1221695283 * Math.Pow(green, -0.0755148492);
				if(green < 0)
					green = 0;
				if(green > 255)
					green = 255;
			}
			//calc blue
			if (temp >= 66)
			{
				blue = 255;
			}
			else {
				if (temp <= 19)
				{
					blue = 0;
				}
				else
				{
					blue = temp - 10;
					blue = 138.5177312231 * Math.Log(blue) - 305.0447927307;
					if(blue < 0)
						blue = 0;
					if(blue > 255)
						blue = 255;
				}
			}
			return new[] {(int) red, (int) green, (int) blue, 255};
		}

		// gets color from the color dialog
		public int[] GetColorPicker() {
			Color c = colorPicker.Color;
			return new int[] {c.R, c.G, c.B, c.A};
		}

		public int[] GetColorPickerCustomColors() {
			int[] colors = colorPicker.CustomColors;
			int r, g, b, a;
			List<int> toReturn = new List<int>();
			toReturn.Add(colors.Length);
			foreach (int color in colors)
			{
				Color c = Color.FromArgb(color);
				r = c.R;
				g = c.G;
				b = c.B;
				a = c.A;
				toReturn.AddRange(new []{r,g,b,a});
			}
			return toReturn.ToArray();
		}

		public int[] GetColorModWheel() {
			decimal modRed = numModifierRed.Value;
			decimal modGreen = numModifierGreen.Value;
			decimal modBlue = numModifierBlue.Value;
			decimal a = numModifier.Value;

			int valR = MaxOrLess((int)(a * modRed), 255);
			int valG = MaxOrLess((int)(a * modGreen), 255);
			int valB = MaxOrLess((int)(a * modBlue), 255);

			return new[] {valR, valG, valB, 255};
		}

		public int[] GetAmbCurve() {
			decimal modRed = numModifierAmbRed.Value;
			decimal modGreen = numModifierAmbGreen.Value;
			decimal modBlue = numModifierAmbBlue.Value;
			decimal a = numModifierAmb.Value;

			int valR = MaxOrLess((int) (a*modRed), 255);
			int valG = MaxOrLess((int) (a*modGreen), 255);
			int valB = MaxOrLess((int) (a*modBlue), 255);

			return new[] { valR,valG,valB, 255 };
		}

		public int[] GetModWheelValues() {
			int mod = (int)numModifier.Value;
			int modR = (int)(numModifierRed.Value*100);
			int modG = (int)(numModifierGreen.Value*100);
			int modB = (int)(numModifierBlue.Value*100);
			return new[] {mod, modR, modG, modB};
		}

		public int[] GetModWheelAmbValues() {
			int mod = (int)numModifierAmb.Value;
			int modR = (int)(numModifierAmbRed.Value * 100);
			int modG = (int)(numModifierAmbGreen.Value * 100);
			int modB = (int)(numModifierAmbBlue.Value * 100);
			return new[] { mod, modR, modG, modB };
		}

		public int[] GetAmbWidth() {
			int ambR = (int) numAmbRed.Value;
			int ambG = (int) numAmbGreen.Value;
			int ambB = (int) numAmbBlue.Value;
			int ambA = (int) numAmbAlpha.Value;
			int width = (int) numWidth.Value;
			return new[] {ambR, ambG, ambB, ambA, width};
		}

		public int MaxOrLess(int toCheck, int max) {
			return toCheck < max ? toCheck : max;
		}

		// keeping various trackbars and numericUpDowns in sync
		private void trackRed_ValueChanged(object sender, EventArgs e) {
			UpdatePair(false,numRed,trackRed);
			UpdateRGBColor();
		}

		private void trackGreen_ValueChanged(object sender, EventArgs e) {
			UpdatePair(false, numGreen, trackGreen);
			UpdateRGBColor();
		}

		private void trackBlue_ValueChanged(object sender, EventArgs e) {
			UpdatePair(false, numBlue, trackBlue);
			UpdateRGBColor();
		}

		private void trackBright_ValueChanged(object sender, EventArgs e) {
			UpdatePair(false, numAlpha, trackBright);
			UpdateRGBColor();
		}

		private void numRed_ValueChanged(object sender, EventArgs e) {
			UpdatePair(true, numRed, trackRed);
			UpdateRGBColor();
		}

		private void numGreen_ValueChanged(object sender, EventArgs e) {
			UpdatePair(true, numGreen, trackGreen);
			UpdateRGBColor();
		}

		private void numBlue_ValueChanged(object sender, EventArgs e) {
			UpdatePair(true, numBlue, trackBlue);
			UpdateRGBColor();
		}

		private void numAlpha_ValueChanged(object sender, EventArgs e) {
			UpdatePair(true, numAlpha, trackBright);
			UpdateRGBColor();
		}

		private void trackWhite_ValueChanged(object sender, EventArgs e) {
			UpdatePair(false, numWhite, trackWhite);
			UpdateWhiteColor();
		}

		private void numWhite_ValueChanged(object sender, EventArgs e) {
			UpdatePair(true, numWhite,trackWhite);
			UpdateWhiteColor();
		}

		private void buttColorPicker_Click(object sender, EventArgs e) {
			colorPicker.ShowDialog();
			UpdateColorPickerColor();
		}

		private void button1_Click(object sender, EventArgs e) {
//			MessageBox.Show(string.Join(",", GetColor()));
			WriteToArduino();
		}

		private void ValueChanged(object sender, EventArgs e) {
			WriteToArduino();
		}

		// Arduino Stuff
		public void WriteToArduino() {
			int[] rgba = GetColor();
			int[] ambWidth;
			if (tabControl1.SelectedIndex == 3)
				ambWidth = GetAmbCurve();
			else
				ambWidth = GetAmbWidth();
			if (!serialPort1.IsOpen)
			{
				try
				{
//					string toSend = "$" + string.Join(",",rgba);
					string toSend = "$";
					toSend += JoinArray(rgba);
//					toSend += JoinArray(ambWidth).Substring(0,JoinArray(ambWidth).Length-1)+ "*";
					toSend += JoinArray(ambWidth) + numWidth.Value + "*";
					if (true)
					{
						WriteToDebugWindow("About to Open port");
						serialPort1.Open();
						WriteToDebugWindow("Port opened. About to write.");
						serialPort1.Write(toSend);
						WriteToDebugWindow("Wrote to Serial. About to close.");
						serialPort1.Close();
						WriteToDebugWindow("Port closed");
					}
					else
					{
						WriteToDebugWindow(toSend);
					}
				}
				catch (Exception e)
				{
					WriteToDebugWindow("Failed to send info to arduino. Show this to maintainer: " + e.ToString());
				}
				serialPort1.Close();
			}
		}

		public string PadZereos(int toPad) {
			string toReturn = toPad.ToString().PadLeft(3, '0');
			return toReturn;
		}

		public string JoinArray(int[] arr) {
			string toReturn = "";
			foreach (int i in arr)
			{
				toReturn += PadZereos(i);
				toReturn += ",";
			}
			return toReturn;
		}

		private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e) {
			MessageBox.Show("Received data");
			
			SerialPort sp = (SerialPort)sender;
			string indata = sp.ReadExisting();
			textBoxDebug.Text += indata;
		}

		public void SaveSettings() {
			UpdateSettings();
			if (currFileSave == null) {
				// prompt for a file
				SaveAs();
				if (currFileSave == null)
					return;
			}
			try {
				File.WriteAllText(currFileSave, ConvertToJson());
				savedSettings = currSettings;
			}
			catch (Exception e) {
				MessageBox.Show(e.ToString());
				Console.WriteLine(e.ToString());
				return;
			}



		}

		public string ConvertToJson() {
			return JsonConvert.SerializeObject(currSettings);
		}

		public void ParseJson(string str) {
			savedSettings = JsonConvert.DeserializeObject<Dictionary<string, int[]>>(str);
			if (savedSettings == null)
			{
				WriteToDebugWindow("Error loading settings");
				return;
			}
		}

		public void UpdateSettings() {
			int[] rgbSlider = GetColorRGBSlider();
			int[] whiteTemp = GetColorWhiteTemp();
			int[] colorPicker = GetColorPicker();
			int[] colorPickerCustoms = GetColorPickerCustomColors();
			int[] modWheel = GetColorModWheel();
			int[] modWheelAmb = GetAmbCurve();
			int[] ambWidth = GetAmbWidth();
			int[] modWheelVals = GetModWheelValues();
			int[] modWheelAmbVals = GetModWheelAmbValues();
			
			currSettings[ArcConstants.RgbSlider] = rgbSlider;
			currSettings[ArcConstants.WhiteTemp] = whiteTemp;
			currSettings[ArcConstants.WhiteTempValue] = new int[] { (int)numWhite.Value };
			currSettings[ArcConstants.ColorPicker] = colorPicker;
			currSettings[ArcConstants.ColorPickCustoms] = colorPickerCustoms;
			currSettings[ArcConstants.ModWheel] = modWheel;
			currSettings[ArcConstants.ModWheelAmb] = modWheelAmb;
			currSettings[ArcConstants.AmbWidth] = ambWidth;
			currSettings[ArcConstants.ModWheelValues] = modWheelVals;
			currSettings[ArcConstants.ModWheelAmbValues] = modWheelAmbVals;

		}

		public void UpdateControls() {
			//update rgbslider
			int[] rgb = currSettings[ArcConstants.RgbSlider];
			numRed.Value = rgb[0];
			numGreen.Value = rgb[1];
			numBlue.Value = rgb[2];
			numAlpha.Value = rgb[3];
			UpdateRGBColor();
			//update whitetemps
			int[] white = currSettings[ArcConstants.WhiteTempValue];
			numWhite.Value = white[0];
			UpdateWhiteColor();
			//update colorPicker
			rgb = currSettings[ArcConstants.ColorPicker];
			colorPicker.Color = Color.FromArgb(rgb[3], rgb[0], rgb[1], rgb[2]);
			//update colorPickerCustoms
			int[] customColors = currSettings[ArcConstants.ColorPickCustoms];
			int numbCustoms = customColors[0];
			for (int i = 1; i < numbCustoms; ++i) {
				//use i as an offset
				int j = i*4;
				Color c = Color.FromArgb(customColors[j + 3], customColors[j], customColors[j + 1], customColors[j + 2]);
				colorPicker.CustomColors[i] = c.ToArgb();
			}
//			colorPicker.CustomColors = (int[]) customColors.Skip(1);
			//update modWheel
			int[] modValues = currSettings[ArcConstants.ModWheelValues];
			numModifier.Value = (decimal) (modValues[0]);
			numModifierRed.Value = (decimal)(modValues[1] / 100.0);
			numModifierGreen.Value = (decimal)(modValues[2] / 100.0);
			numModifierBlue.Value = (decimal)(modValues[3] / 100.0);
//			//update modWheelAmb
			int[] modAmbValues = currSettings[ArcConstants.ModWheelAmbValues];
			numModifierAmb.Value = (decimal)(modAmbValues[0]);
			numModifierAmbRed.Value = (decimal)(modAmbValues[1] / 100.0);
			numModifierAmbGreen.Value = (decimal)(modAmbValues[2] / 100.0);
			numModifierAmbBlue.Value = (decimal)(modAmbValues[3] / 100.0);
			//update ambWidth
			int[] ambWidth = currSettings[ArcConstants.AmbWidth];
			numAmbRed.Value = ambWidth[0];
			numAmbGreen.Value = ambWidth[1];
			numAmbBlue.Value = ambWidth[2];
			numAmbAlpha.Value = ambWidth[3];
			numWidth.Value = ambWidth[4];
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
			if (JsonConvert.SerializeObject(currSettings).Equals(JsonConvert.SerializeObject(savedSettings)))
			{
				return;
			}
			// ask if they want to save
			// if yes save


			DialogResult dr = DialogResult.Cancel;
			if (dr == DialogResult.OK) {
				SaveSettings();
			}

		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
			Console.WriteLine(JsonConvert.SerializeObject(currSettings));
			if (savedSettings == null) {
				// prompt save as instead
				SaveAs();
			}
			else {
				SaveSettings();
			}
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
			SaveAs();
		}

		private void SaveAs() {
			SaveFileDialog d = new SaveFileDialog();
			d.Filter = @"Arc Color File (*.arcc) | *.arcc";
			DialogResult dR = d.ShowDialog();
			if(dR != DialogResult.OK) {
				WriteToDebugWindow("File Not Saved!");
			}
			currFileSave = d.FileName;
			SaveSettings();
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e) {
			LoadSettings();
		}

		private void LoadSettings() {
			OpenFileDialog d = new OpenFileDialog();
			d.Filter = @"Arc Color File (*.arcc) | *.arcc";
			DialogResult dR = d.ShowDialog();
			if (dR != DialogResult.OK) {
				WriteToDebugWindow("File not Selected");
				return;
			}
			currFileSave = d.FileName;

			try {
//				Console.WriteLine("About to read");
				string contents = File.ReadAllText(currFileSave);
//				Console.WriteLine("read file");
				ParseJson(contents);
//				Console.WriteLine("Parsed");
				currSettings = savedSettings;
				UpdateControls();
			}
			catch (Exception e) {
				WriteToDebugWindow("Error loading File");
				Console.WriteLine(e.ToString());
			}
		}

		private void testToolStripMenuItem_Click(object sender, EventArgs e) {
			string str = File.ReadAllText(currFileSave);
			Console.WriteLine(str);
			Console.WriteLine(JsonConvert.DeserializeObject<Dictionary<string,int[]>>(str));
		}

		public void WriteToDebugWindow(string toWrite) {
			textBoxDebug.AppendText(toWrite + "\r\n");
		}

		public string AutodetectArduinoPort() {
			ManagementScope connectionScope = new ManagementScope();
			SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
			ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

			try {
				foreach(ManagementObject item in searcher.Get()) {
					string desc = item["Description"].ToString();
					string deviceId = item["DeviceID"].ToString();

					if(desc.Contains("Arduino")) {
						return deviceId;
					}
				}
			}
			catch(ManagementException e) {
				/* Do Nothing */
			}

			return null;
		}
	}



	public struct ArcConstants {
		public static string RgbSlider = "rgbSlider";
		public static string WhiteTemp = "whiteTemp";
		public static string WhiteTempValue = "whiteTempValue";
		public static string ColorPicker = "colorPicker";
		public static string ColorPickCustoms = "ColorPickCustoms";
		public static string ModWheel = "modWheel";
		public static string ModWheelAmb = "modWheelAmb";
		public static string ModWheelValues = "modWheelValues";
		public static string ModWheelAmbValues = "modWheelAmbValues";
		public static string AmbWidth = "ambWidth";

	}

}
