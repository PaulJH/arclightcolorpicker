﻿namespace Software_Interface {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.numGreen = new System.Windows.Forms.NumericUpDown();
			this.numBlue = new System.Windows.Forms.NumericUpDown();
			this.numAlpha = new System.Windows.Forms.NumericUpDown();
			this.numRed = new System.Windows.Forms.NumericUpDown();
			this.labBright = new System.Windows.Forms.Label();
			this.labBlue = new System.Windows.Forms.Label();
			this.labGreen = new System.Windows.Forms.Label();
			this.labRed = new System.Windows.Forms.Label();
			this.picRGBDisplayColor = new System.Windows.Forms.PictureBox();
			this.trackBright = new System.Windows.Forms.TrackBar();
			this.trackBlue = new System.Windows.Forms.TrackBar();
			this.trackGreen = new System.Windows.Forms.TrackBar();
			this.trackRed = new System.Windows.Forms.TrackBar();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.picWhiteDisplayColor = new System.Windows.Forms.PictureBox();
			this.numWhite = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.trackWhite = new System.Windows.Forms.TrackBar();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.buttPickColor = new System.Windows.Forms.Button();
			this.picColorPickerDisplayColor = new System.Windows.Forms.PictureBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.label16 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.numModifierAmb = new System.Windows.Forms.NumericUpDown();
			this.numModifierAmbGreen = new System.Windows.Forms.NumericUpDown();
			this.numModifierAmbBlue = new System.Windows.Forms.NumericUpDown();
			this.numModifierAmbRed = new System.Windows.Forms.NumericUpDown();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.numModifier = new System.Windows.Forms.NumericUpDown();
			this.numModifierGreen = new System.Windows.Forms.NumericUpDown();
			this.numModifierBlue = new System.Windows.Forms.NumericUpDown();
			this.numModifierRed = new System.Windows.Forms.NumericUpDown();
			this.button1 = new System.Windows.Forms.Button();
			this.colorPicker = new System.Windows.Forms.ColorDialog();
			this.textBoxDebug = new System.Windows.Forms.TextBox();
			this.numAmbBlue = new System.Windows.Forms.NumericUpDown();
			this.numAmbGreen = new System.Windows.Forms.NumericUpDown();
			this.numAmbAlpha = new System.Windows.Forms.NumericUpDown();
			this.numWidth = new System.Windows.Forms.NumericUpDown();
			this.numAmbRed = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numGreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numBlue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numAlpha)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picRGBDisplayColor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBright)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBlue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackGreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackRed)).BeginInit();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picWhiteDisplayColor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numWhite)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackWhite)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picColorPickerDisplayColor)).BeginInit();
			this.tabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmb)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbGreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbBlue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbRed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierGreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierBlue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierRed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbBlue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbGreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbAlpha)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbRed)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Location = new System.Drawing.Point(12, 268);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(762, 413);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.numGreen);
			this.tabPage1.Controls.Add(this.numBlue);
			this.tabPage1.Controls.Add(this.numAlpha);
			this.tabPage1.Controls.Add(this.numRed);
			this.tabPage1.Controls.Add(this.labBright);
			this.tabPage1.Controls.Add(this.labBlue);
			this.tabPage1.Controls.Add(this.labGreen);
			this.tabPage1.Controls.Add(this.labRed);
			this.tabPage1.Controls.Add(this.picRGBDisplayColor);
			this.tabPage1.Controls.Add(this.trackBright);
			this.tabPage1.Controls.Add(this.trackBlue);
			this.tabPage1.Controls.Add(this.trackGreen);
			this.tabPage1.Controls.Add(this.trackRed);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(754, 387);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "RGB Sliders";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// numGreen
			// 
			this.numGreen.Location = new System.Drawing.Point(76, 34);
			this.numGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numGreen.Name = "numGreen";
			this.numGreen.Size = new System.Drawing.Size(45, 20);
			this.numGreen.TabIndex = 34;
			this.numGreen.ValueChanged += new System.EventHandler(this.numGreen_ValueChanged);
			// 
			// numBlue
			// 
			this.numBlue.Location = new System.Drawing.Point(145, 34);
			this.numBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numBlue.Name = "numBlue";
			this.numBlue.Size = new System.Drawing.Size(45, 20);
			this.numBlue.TabIndex = 33;
			this.numBlue.ValueChanged += new System.EventHandler(this.numBlue_ValueChanged);
			// 
			// numAlpha
			// 
			this.numAlpha.Location = new System.Drawing.Point(214, 34);
			this.numAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAlpha.Name = "numAlpha";
			this.numAlpha.Size = new System.Drawing.Size(45, 20);
			this.numAlpha.TabIndex = 32;
			this.numAlpha.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAlpha.ValueChanged += new System.EventHandler(this.numAlpha_ValueChanged);
			// 
			// numRed
			// 
			this.numRed.Location = new System.Drawing.Point(7, 34);
			this.numRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numRed.Name = "numRed";
			this.numRed.Size = new System.Drawing.Size(45, 20);
			this.numRed.TabIndex = 31;
			this.numRed.ValueChanged += new System.EventHandler(this.numRed_ValueChanged);
			// 
			// labBright
			// 
			this.labBright.AutoSize = true;
			this.labBright.Location = new System.Drawing.Point(215, 365);
			this.labBright.Name = "labBright";
			this.labBright.Size = new System.Drawing.Size(34, 13);
			this.labBright.TabIndex = 26;
			this.labBright.Text = "Alpha";
			// 
			// labBlue
			// 
			this.labBlue.AutoSize = true;
			this.labBlue.Location = new System.Drawing.Point(150, 365);
			this.labBlue.Name = "labBlue";
			this.labBlue.Size = new System.Drawing.Size(28, 13);
			this.labBlue.TabIndex = 25;
			this.labBlue.Text = "Blue";
			// 
			// labGreen
			// 
			this.labGreen.AutoSize = true;
			this.labGreen.Location = new System.Drawing.Point(81, 365);
			this.labGreen.Name = "labGreen";
			this.labGreen.Size = new System.Drawing.Size(36, 13);
			this.labGreen.TabIndex = 24;
			this.labGreen.Text = "Green";
			// 
			// labRed
			// 
			this.labRed.AutoSize = true;
			this.labRed.Location = new System.Drawing.Point(12, 365);
			this.labRed.Name = "labRed";
			this.labRed.Size = new System.Drawing.Size(27, 13);
			this.labRed.TabIndex = 23;
			this.labRed.Text = "Red";
			// 
			// picRGBDisplayColor
			// 
			this.picRGBDisplayColor.BackColor = System.Drawing.Color.Black;
			this.picRGBDisplayColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picRGBDisplayColor.Location = new System.Drawing.Point(289, 168);
			this.picRGBDisplayColor.Name = "picRGBDisplayColor";
			this.picRGBDisplayColor.Size = new System.Drawing.Size(100, 100);
			this.picRGBDisplayColor.TabIndex = 22;
			this.picRGBDisplayColor.TabStop = false;
			// 
			// trackBright
			// 
			this.trackBright.Location = new System.Drawing.Point(214, 60);
			this.trackBright.Maximum = 255;
			this.trackBright.Name = "trackBright";
			this.trackBright.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackBright.Size = new System.Drawing.Size(45, 302);
			this.trackBright.TabIndex = 21;
			this.trackBright.Value = 255;
			this.trackBright.ValueChanged += new System.EventHandler(this.trackBright_ValueChanged);
			// 
			// trackBlue
			// 
			this.trackBlue.Location = new System.Drawing.Point(145, 60);
			this.trackBlue.Maximum = 255;
			this.trackBlue.Name = "trackBlue";
			this.trackBlue.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackBlue.Size = new System.Drawing.Size(45, 302);
			this.trackBlue.TabIndex = 20;
			this.trackBlue.ValueChanged += new System.EventHandler(this.trackBlue_ValueChanged);
			// 
			// trackGreen
			// 
			this.trackGreen.Location = new System.Drawing.Point(76, 60);
			this.trackGreen.Maximum = 255;
			this.trackGreen.Name = "trackGreen";
			this.trackGreen.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackGreen.Size = new System.Drawing.Size(45, 302);
			this.trackGreen.TabIndex = 19;
			this.trackGreen.ValueChanged += new System.EventHandler(this.trackGreen_ValueChanged);
			// 
			// trackRed
			// 
			this.trackRed.Location = new System.Drawing.Point(7, 60);
			this.trackRed.Maximum = 255;
			this.trackRed.Name = "trackRed";
			this.trackRed.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackRed.Size = new System.Drawing.Size(45, 302);
			this.trackRed.TabIndex = 18;
			this.trackRed.ValueChanged += new System.EventHandler(this.trackRed_ValueChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.picWhiteDisplayColor);
			this.tabPage2.Controls.Add(this.numWhite);
			this.tabPage2.Controls.Add(this.label2);
			this.tabPage2.Controls.Add(this.label1);
			this.tabPage2.Controls.Add(this.trackWhite);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(754, 387);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "White Temps";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// picWhiteDisplayColor
			// 
			this.picWhiteDisplayColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(67)))), ((int)(((byte)(0)))));
			this.picWhiteDisplayColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picWhiteDisplayColor.Location = new System.Drawing.Point(314, 147);
			this.picWhiteDisplayColor.Name = "picWhiteDisplayColor";
			this.picWhiteDisplayColor.Size = new System.Drawing.Size(100, 100);
			this.picWhiteDisplayColor.TabIndex = 5;
			this.picWhiteDisplayColor.TabStop = false;
			// 
			// numWhite
			// 
			this.numWhite.Location = new System.Drawing.Point(304, 80);
			this.numWhite.Maximum = new decimal(new int[] {
            15000,
            0,
            0,
            0});
			this.numWhite.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numWhite.Name = "numWhite";
			this.numWhite.Size = new System.Drawing.Size(120, 20);
			this.numWhite.TabIndex = 3;
			this.numWhite.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numWhite.ValueChanged += new System.EventHandler(this.numWhite_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(647, 82);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "15000k";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(44, 82);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(37, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "1000k";
			// 
			// trackWhite
			// 
			this.trackWhite.Location = new System.Drawing.Point(47, 34);
			this.trackWhite.Maximum = 15000;
			this.trackWhite.Minimum = 1000;
			this.trackWhite.Name = "trackWhite";
			this.trackWhite.Size = new System.Drawing.Size(635, 45);
			this.trackWhite.TabIndex = 0;
			this.trackWhite.Value = 1000;
			this.trackWhite.ValueChanged += new System.EventHandler(this.trackWhite_ValueChanged);
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.buttPickColor);
			this.tabPage3.Controls.Add(this.picColorPickerDisplayColor);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(754, 387);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Color Picker";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// buttPickColor
			// 
			this.buttPickColor.Location = new System.Drawing.Point(212, 34);
			this.buttPickColor.Name = "buttPickColor";
			this.buttPickColor.Size = new System.Drawing.Size(100, 100);
			this.buttPickColor.TabIndex = 3;
			this.buttPickColor.Text = "Pick Color";
			this.buttPickColor.UseVisualStyleBackColor = true;
			this.buttPickColor.Click += new System.EventHandler(this.buttColorPicker_Click);
			// 
			// picColorPickerDisplayColor
			// 
			this.picColorPickerDisplayColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picColorPickerDisplayColor.Location = new System.Drawing.Point(343, 34);
			this.picColorPickerDisplayColor.Name = "picColorPickerDisplayColor";
			this.picColorPickerDisplayColor.Size = new System.Drawing.Size(100, 100);
			this.picColorPickerDisplayColor.TabIndex = 2;
			this.picColorPickerDisplayColor.TabStop = false;
			this.picColorPickerDisplayColor.Click += new System.EventHandler(this.buttColorPicker_Click);
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.label16);
			this.tabPage4.Controls.Add(this.label12);
			this.tabPage4.Controls.Add(this.label13);
			this.tabPage4.Controls.Add(this.label14);
			this.tabPage4.Controls.Add(this.label15);
			this.tabPage4.Controls.Add(this.numModifierAmb);
			this.tabPage4.Controls.Add(this.numModifierAmbGreen);
			this.tabPage4.Controls.Add(this.numModifierAmbBlue);
			this.tabPage4.Controls.Add(this.numModifierAmbRed);
			this.tabPage4.Controls.Add(this.label11);
			this.tabPage4.Controls.Add(this.label10);
			this.tabPage4.Controls.Add(this.label9);
			this.tabPage4.Controls.Add(this.label8);
			this.tabPage4.Controls.Add(this.numModifier);
			this.tabPage4.Controls.Add(this.numModifierGreen);
			this.tabPage4.Controls.Add(this.numModifierBlue);
			this.tabPage4.Controls.Add(this.numModifierRed);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(754, 387);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Mod Wheel";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(313, 139);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(45, 13);
			this.label16.TabIndex = 16;
			this.label16.Text = "Ambient";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(491, 165);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(66, 13);
			this.label12.TabIndex = 15;
			this.label12.Text = "Blue Modifer";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(361, 165);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(74, 13);
			this.label13.TabIndex = 14;
			this.label13.Text = "Green Modifer";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(239, 165);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(67, 13);
			this.label14.TabIndex = 13;
			this.label14.Text = "Red Modifier";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(124, 165);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(44, 13);
			this.label15.TabIndex = 12;
			this.label15.Text = "Modifier";
			// 
			// numModifierAmb
			// 
			this.numModifierAmb.Location = new System.Drawing.Point(86, 181);
			this.numModifierAmb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numModifierAmb.Name = "numModifierAmb";
			this.numModifierAmb.Size = new System.Drawing.Size(120, 20);
			this.numModifierAmb.TabIndex = 11;
			// 
			// numModifierAmbGreen
			// 
			this.numModifierAmbGreen.DecimalPlaces = 2;
			this.numModifierAmbGreen.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierAmbGreen.Location = new System.Drawing.Point(338, 181);
			this.numModifierAmbGreen.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierAmbGreen.Name = "numModifierAmbGreen";
			this.numModifierAmbGreen.Size = new System.Drawing.Size(120, 20);
			this.numModifierAmbGreen.TabIndex = 10;
			this.numModifierAmbGreen.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// numModifierAmbBlue
			// 
			this.numModifierAmbBlue.DecimalPlaces = 2;
			this.numModifierAmbBlue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierAmbBlue.Location = new System.Drawing.Point(464, 181);
			this.numModifierAmbBlue.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierAmbBlue.Name = "numModifierAmbBlue";
			this.numModifierAmbBlue.Size = new System.Drawing.Size(120, 20);
			this.numModifierAmbBlue.TabIndex = 9;
			this.numModifierAmbBlue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// numModifierAmbRed
			// 
			this.numModifierAmbRed.DecimalPlaces = 2;
			this.numModifierAmbRed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierAmbRed.Location = new System.Drawing.Point(212, 181);
			this.numModifierAmbRed.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierAmbRed.Name = "numModifierAmbRed";
			this.numModifierAmbRed.Size = new System.Drawing.Size(120, 20);
			this.numModifierAmbRed.TabIndex = 8;
			this.numModifierAmbRed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(491, 48);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(66, 13);
			this.label11.TabIndex = 7;
			this.label11.Text = "Blue Modifer";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(361, 48);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(74, 13);
			this.label10.TabIndex = 6;
			this.label10.Text = "Green Modifer";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(239, 48);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(67, 13);
			this.label9.TabIndex = 5;
			this.label9.Text = "Red Modifier";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(124, 48);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(44, 13);
			this.label8.TabIndex = 4;
			this.label8.Text = "Modifier";
			// 
			// numModifier
			// 
			this.numModifier.Location = new System.Drawing.Point(86, 64);
			this.numModifier.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numModifier.Name = "numModifier";
			this.numModifier.Size = new System.Drawing.Size(120, 20);
			this.numModifier.TabIndex = 3;
			// 
			// numModifierGreen
			// 
			this.numModifierGreen.DecimalPlaces = 2;
			this.numModifierGreen.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierGreen.Location = new System.Drawing.Point(338, 64);
			this.numModifierGreen.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierGreen.Name = "numModifierGreen";
			this.numModifierGreen.Size = new System.Drawing.Size(120, 20);
			this.numModifierGreen.TabIndex = 2;
			this.numModifierGreen.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// numModifierBlue
			// 
			this.numModifierBlue.DecimalPlaces = 2;
			this.numModifierBlue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierBlue.Location = new System.Drawing.Point(464, 64);
			this.numModifierBlue.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierBlue.Name = "numModifierBlue";
			this.numModifierBlue.Size = new System.Drawing.Size(120, 20);
			this.numModifierBlue.TabIndex = 1;
			this.numModifierBlue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// numModifierRed
			// 
			this.numModifierRed.DecimalPlaces = 2;
			this.numModifierRed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numModifierRed.Location = new System.Drawing.Point(212, 64);
			this.numModifierRed.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numModifierRed.Name = "numModifierRed";
			this.numModifierRed.Size = new System.Drawing.Size(120, 20);
			this.numModifierRed.TabIndex = 0;
			this.numModifierRed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(650, 175);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(110, 58);
			this.button1.TabIndex = 1;
			this.button1.Text = "Set Color";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// colorPicker
			// 
			this.colorPicker.AnyColor = true;
			this.colorPicker.FullOpen = true;
			// 
			// textBoxDebug
			// 
			this.textBoxDebug.Location = new System.Drawing.Point(23, 48);
			this.textBoxDebug.Multiline = true;
			this.textBoxDebug.Name = "textBoxDebug";
			this.textBoxDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxDebug.Size = new System.Drawing.Size(489, 214);
			this.textBoxDebug.TabIndex = 2;
			// 
			// numAmbBlue
			// 
			this.numAmbBlue.Location = new System.Drawing.Point(650, 119);
			this.numAmbBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAmbBlue.Name = "numAmbBlue";
			this.numAmbBlue.Size = new System.Drawing.Size(120, 20);
			this.numAmbBlue.TabIndex = 3;
			// 
			// numAmbGreen
			// 
			this.numAmbGreen.Location = new System.Drawing.Point(650, 93);
			this.numAmbGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAmbGreen.Name = "numAmbGreen";
			this.numAmbGreen.Size = new System.Drawing.Size(120, 20);
			this.numAmbGreen.TabIndex = 4;
			// 
			// numAmbAlpha
			// 
			this.numAmbAlpha.Location = new System.Drawing.Point(650, 145);
			this.numAmbAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAmbAlpha.Name = "numAmbAlpha";
			this.numAmbAlpha.Size = new System.Drawing.Size(120, 20);
			this.numAmbAlpha.TabIndex = 6;
			// 
			// numWidth
			// 
			this.numWidth.Location = new System.Drawing.Point(647, 242);
			this.numWidth.Maximum = new decimal(new int[] {
            288,
            0,
            0,
            0});
			this.numWidth.Name = "numWidth";
			this.numWidth.Size = new System.Drawing.Size(120, 20);
			this.numWidth.TabIndex = 5;
			// 
			// numAmbRed
			// 
			this.numAmbRed.Location = new System.Drawing.Point(650, 67);
			this.numAmbRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numAmbRed.Name = "numAmbRed";
			this.numAmbRed.Size = new System.Drawing.Size(120, 20);
			this.numAmbRed.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(571, 75);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Ambient Red";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(567, 101);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Ambient Green";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(571, 127);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(69, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "Ambient Blue";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(568, 153);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(75, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "Ambient Alpha";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(585, 249);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(35, 13);
			this.label7.TabIndex = 12;
			this.label7.Text = "Width";
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
			this.menuStrip1.Size = new System.Drawing.Size(786, 24);
			this.menuStrip1.TabIndex = 13;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.loadToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.saveAsToolStripMenuItem.Text = "Save As";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.loadToolStripMenuItem.Text = "Load";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(786, 700);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numAmbRed);
			this.Controls.Add(this.numAmbAlpha);
			this.Controls.Add(this.numWidth);
			this.Controls.Add(this.numAmbGreen);
			this.Controls.Add(this.numAmbBlue);
			this.Controls.Add(this.textBoxDebug);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "Arduino Color Selector";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numGreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numBlue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numAlpha)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picRGBDisplayColor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBright)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBlue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackGreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackRed)).EndInit();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picWhiteDisplayColor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numWhite)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackWhite)).EndInit();
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.picColorPickerDisplayColor)).EndInit();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmb)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbGreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbBlue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierAmbRed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierGreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierBlue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numModifierRed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbBlue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbGreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbAlpha)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numAmbRed)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.NumericUpDown numRed;
		private System.Windows.Forms.Label labBright;
		private System.Windows.Forms.Label labBlue;
		private System.Windows.Forms.Label labGreen;
		private System.Windows.Forms.Label labRed;
		private System.Windows.Forms.PictureBox picRGBDisplayColor;
		private System.Windows.Forms.TrackBar trackBright;
		private System.Windows.Forms.TrackBar trackBlue;
		private System.Windows.Forms.TrackBar trackGreen;
		private System.Windows.Forms.TrackBar trackRed;
		private System.Windows.Forms.NumericUpDown numGreen;
		private System.Windows.Forms.NumericUpDown numBlue;
		private System.Windows.Forms.NumericUpDown numAlpha;
		private System.Windows.Forms.TrackBar trackWhite;
		private System.Windows.Forms.NumericUpDown numWhite;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox picWhiteDisplayColor;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ColorDialog colorPicker;
		private System.Windows.Forms.Button buttPickColor;
		private System.Windows.Forms.PictureBox picColorPickerDisplayColor;
		private System.Windows.Forms.TextBox textBoxDebug;
		private System.Windows.Forms.NumericUpDown numAmbBlue;
		private System.Windows.Forms.NumericUpDown numAmbGreen;
		private System.Windows.Forms.NumericUpDown numAmbAlpha;
		private System.Windows.Forms.NumericUpDown numWidth;
		private System.Windows.Forms.NumericUpDown numAmbRed;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown numModifier;
		private System.Windows.Forms.NumericUpDown numModifierGreen;
		private System.Windows.Forms.NumericUpDown numModifierBlue;
		private System.Windows.Forms.NumericUpDown numModifierRed;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.NumericUpDown numModifierAmb;
		private System.Windows.Forms.NumericUpDown numModifierAmbGreen;
		private System.Windows.Forms.NumericUpDown numModifierAmbBlue;
		private System.Windows.Forms.NumericUpDown numModifierAmbRed;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
	}
}

